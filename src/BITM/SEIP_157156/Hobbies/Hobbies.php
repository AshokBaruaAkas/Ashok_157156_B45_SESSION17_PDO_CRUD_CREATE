<?php

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobbies extends DB{
    private $id;
    private $name;
    private $hobbies;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("hobie1",$allPostData)){
            $this->hobbies = $this->hobbies.$allPostData['hobie1'].",";
        }
        if(array_key_exists("hobie2",$allPostData)){
            $this->hobbies = $this->hobbies.$allPostData['hobie2'].",";
        }
        if(array_key_exists("hobie3",$allPostData)){
            $this->hobbies = $this->hobbies.$allPostData['hobie3'].",";
        }
        if(array_key_exists("hobie4",$allPostData)){
            $this->hobbies = $this->hobbies.$allPostData['hobie4'].",";
        }
        if(array_key_exists("hobie5",$allPostData)){
            $this->hobbies = $this->hobbies.$allPostData['hobie5'].",";
        }
        if(isset($this->hobbies)){
            $this->hobbies = substr($this->hobbies, 0, -1).".";
        }
    }

    public function store()
    {
        $arrayData = array($this->name,$this->hobbies);
        $query = 'INSERT INTO hobbies (name, hobbies) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        Utility::redirect('create.php');
    }
}