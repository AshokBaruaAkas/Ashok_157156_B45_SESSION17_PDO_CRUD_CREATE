<?php

namespace App\BirthDay;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BirthDay extends DB
{
    private $id;
    private $name;
    private $birthDate;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("birthDate",$allPostData)){
            $this->birthDate = $allPostData['birthDate'];
        }
    }

    public function store()
    {
        $arrayData = array($this->name,$this->birthDate);
        $query = 'INSERT INTO birth_day (name, birthDate) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        Utility::redirect('create.php');
    }
}